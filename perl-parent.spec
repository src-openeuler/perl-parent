%global perl_package_name parent
Name:		perl-parent
Epoch:		1
Version:	0.242
Release:	2
Summary:	Establish an ISA relationship with base classes at compile time
License:	GPL-1.0-or-later OR Artistic-1.0-Perl
URL:		https://metacpan.org/release/parent
Source0:	https://cpan.metacpan.org/authors/id/C/CO/CORION/%{perl_package_name}-%{version}.tar.gz
BuildArch:	noarch

BuildRequires:	gcc make
BuildRequires:	perl-interpreter
BuildRequires:	perl-generators
BuildRequires:	perl(ExtUtils::MakeMaker)
BuildRequires:	perl(Test::More) >= 0.40

%description
Allows you to both load one or more modules, while setting up inheritance
from those modules at the same time.

%package_help

%prep
%autosetup -n %{perl_package_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} -c $RPM_BUILD_ROOT

%check
make test

%files
%doc Changes
%{perl_vendorlib}/parent.pm

%files help
%{_mandir}/man3/parent.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1:0.242-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Nov 06 2024 zhaosaisai <zhaosaisai@kylinos.cn> - 1:0.242-1
- Upgrade to the latest version 0.242
- Don't test for apostrophe as package separator on Perl versions after 5.41

* Fri Jul 21 2023 xujing <xujing125@huawei.com> - 1:0.241-1
- update version to 0.241

* Mon Aug 02 2021 chenyanpanHW <chenyanpan@huawei.com> - 1:0.238-2
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Sat Jul 25 2020 zhanzhimin <zhanzhimin@huawei.com> - 1:0.238-1

* Thu Sep 26 2019 shenyangyang<shenyangyang4@huawei.com> - 1:0.237-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise help package

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:0.237-3
- Package Init
